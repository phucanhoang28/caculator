//
//  ViewController.swift
//  Caculator
//
//  Created by Hoàng Phúc An on 05/12/2017.
//  Copyright © 2017 Hoàng Phúc An. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var a: Double! = 0.0
    var b: Double! = 0.0
    var k: String! = "+"
    var kq: Double!
    var nhapso : Bool = true
    var ketqua : Bool = true
    @IBOutlet weak var lblScreen: UILabel!
    @IBOutlet var allButton: [UIButton]!
    @IBAction func didTouch1(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "1"
        nhapso = true
    }
    @IBAction func didTouch2(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "2"
        nhapso = true
    }
    @IBAction func didTouch3(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "3"
        nhapso = true
    }
    @IBAction func didTouch4(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "4"
        nhapso = true
    }
    @IBAction func didTouch5(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "5"
        nhapso = true
    }
    @IBAction func didTouch6(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "6"
        nhapso = true
    }
    @IBAction func didTouch7(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "7"
        nhapso = true
    }
    @IBAction func didTouch8(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "8"
        nhapso = true
    }
    @IBAction func didTouch9(_ sender: Any) {
        if (nhapso == false) {
            lblScreen.text = ""
        }
        lblScreen.text! += "9"
        nhapso = true
    }
    @IBAction func didTouch0(_ sender: Any) {
        if(lblScreen.text != ""){
        lblScreen.text! += "0"
        }
        if (nhapso == false) {
            lblScreen.text = ""
        }
        nhapso = true
    }
    
    @IBAction func didTouchAdd(_ sender: Any) {

        if(ketqua == false){
            caculator(soa: a, k: k)
            nhapso = false
        }
        a = Double(lblScreen.text!)
        nhapso = false
        k = "+"
        ketqua = false
    }
    @IBAction func didTouchTru(_ sender: Any) {
        if(ketqua == false){
            caculator(soa: a, k: k)
            nhapso = false
        }
        a = Double(lblScreen.text!)
        nhapso = false
        k = "-"
        ketqua = false
    }
    @IBAction func didTouchNhan(_ sender: Any) {
        if(ketqua == false){
            caculator(soa: a, k: k)
            nhapso = false
        }
        a = Double(lblScreen.text!)
        nhapso = false
        k = "*"
        ketqua = false
    }
    
    @IBAction func didTouchChia(_ sender: Any) {
        if(ketqua == false){
            caculator(soa: a, k: k)
            nhapso = false
        }
        a = Double(lblScreen.text!)
        nhapso = false
        k = "/"
        ketqua = false
    }
    @IBAction func didTouchEquals(_ sender: Any) {
        caculator(soa: a, k: k)
        nhapso = false
        
    }
    
    @IBAction func didTouchAC(_ sender: Any) {
        lblScreen.text = ""
        a = 0
        b = 0
        kq = 0
    }
    func caculator( soa : Double!, k : String!)  {
        if(lblScreen.text == ""){
            b = 0
            
        }
        b = Double(lblScreen.text!)
        if (k == "+") {
            kq = soa + b
           // a = kq
        }
        if(k == "-"){
            kq = a - b
        }
        if(k == "*"){
            kq = a * b
        }
        if (k == "/") {
                kq = a / b
        }
        lblScreen.text = String(kq)
        ketqua = true
    }
    func configUI(){
        for item in allButton {
            item.layer.cornerRadius = 5
            item.layer.masksToBounds = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        lblScreen.text = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

